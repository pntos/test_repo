﻿using UnityEngine;
using System.Collections;

/**
 * FpsCounter
 * - Fps utility class
 * autor : Yusuke Fujioka
 * date  : 2014/07/07
 */
public class FpsCounter : MonoBehaviour {
	
	const float UPDATE_INTERVAL = 1.0f; 	// update Interval

	private int m_Frame;					// FrameCount
	private float m_Accumulated;			// Accumulate	
	private float m_TimeUntilNextInterval;	// next time interval

	// Use this for initialization
	void Start () {
		m_TimeUntilNextInterval = UPDATE_INTERVAL;
	}
	
	// Update is called once per frame
	void Update () {
		m_TimeUntilNextInterval -= Time.deltaTime;
		m_Accumulated += Time.timeScale / Time.deltaTime;
		++m_Frame;

		if( m_TimeUntilNextInterval <= 0.0 ) {
			// FPS calculate
			float fps = m_Accumulated / m_Frame;
			string format = System.String.Format( "FPS: {0:F3}", fps );
			guiText.text = format;
			
			m_TimeUntilNextInterval = UPDATE_INTERVAL;
			m_Accumulated = 0.0F;
			m_Frame = 0;
		}
	}
}
