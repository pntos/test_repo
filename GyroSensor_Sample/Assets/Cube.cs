﻿using UnityEngine;
using System.Collections;

public class Cube : MonoBehaviour {

	const float SPEED = 5.0f;	// Speed fix.

	// Use this for initialization
	void Start () {
		Screen.orientation = ScreenOrientation.LandscapeLeft;
	}
	
	// Update is called once per frame
	void Update () {

		// Acceleration Sensor Manager for example
 		AccelerationSensorManager.GetInstance ().Update ();
		Vector3 n_Direction = AccelerationSensorManager.GetInstance ().GetDirection ().normalized;
		float direction_x = AccelerationSensorManager.GetInstance ().GetDirectionX ();

		// Rotation
		transform.Rotate (0, 0, direction_x * 1.4f); 

		// translate
//		float translate_dirction = n_Direction.x;
//		transform.position.x += translate_dirction * SPEED;
	}
}
