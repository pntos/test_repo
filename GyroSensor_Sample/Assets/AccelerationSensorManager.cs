﻿using UnityEngine;
using System.Collections;

/**
 * AccelerationSensorManager
 * - Acceleration Sensor management class.
 * autor : Yusuke Fujioka
 * date  : 2014/07/07
 */
public class AccelerationSensorManager {

	private static AccelerationSensorManager m_Instance;	// Singleton
	private Vector3 m_Direction;							// Direction

	// constructor
	AccelerationSensorManager () {
		m_Direction.Set (0, 0, 0);
	}

	// Singleton method
	public static AccelerationSensorManager GetInstance () {
		if (m_Instance == null) {
			m_Instance = new AccelerationSensorManager ();
		}
		return m_Instance;
	}
	
	// Update is called once per frame
	public void Update () {
		m_Direction.Set (Input.acceleration.x,
		                Input.acceleration.y,
		                Input.acceleration.z);
	}
	
	// get rotation
	public Vector3 GetDirection () {
		return m_Direction;
	}

	// get rotation x
	public float GetDirectionX () {
		return m_Direction.x * -1;
	}

	// get rotation x
	public float GetDirectionY () {
		return m_Direction.y * -1;
	}

	// get rotation x
	public float GetDirectionZ () {
		return m_Direction.z * -1;
	}
}
